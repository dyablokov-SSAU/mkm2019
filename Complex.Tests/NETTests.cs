﻿namespace GenericComplex.Tests
{
    using System;
    using NUnit.Framework;
    using System.Numerics;
    

    [TestFixture]
    public class NETTests
    {
        private const string CATEGORY = "NET";

        [Test]
        [Category(CATEGORY)]
        public void ComplexTest()
        {
            var a = new Complex(1, 9);
            var b = new Complex(4, 2);

            var c = new Complex(1, 9);
            var d = new Complex(6, 7);

            var e = new Complex(1, 9);
            var f = new Complex(9, 2);

            var g = new Complex(2, 0);
            var h = new Complex(1, 5);

            var sum = a + b;
            var diff = c - d;
            var prod = e * f;
            var qout = g / h;
        }
    };
}
