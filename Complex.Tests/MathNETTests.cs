﻿namespace GenericComplex.Tests
{
    using System;
    using NUnit.Framework;
    using MathNet.Numerics;

    [TestFixture]
    public class MathNETTests
    {
        [Test]
        [Category("MathNET")]
        public void Complex32Test()
        {
            var a = new Complex32(1, 9);
            var b = new Complex32(4, 2);

            var c = new Complex32(1, 9);
            var d = new Complex32(6, 7);

            var e = new Complex32(1, 9);
            var f = new Complex32(9, 2);

            var g = new Complex32(2, 0);
            var h = new Complex32(1, 5);

            var sum = a + b;
            var diff = c - d;
            var prod = e * f;
            var qout = g / h;
        }
    };
}
