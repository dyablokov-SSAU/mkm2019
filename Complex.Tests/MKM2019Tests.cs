﻿namespace GenericComplex.Tests
{
    using System;
    using NUnit.Framework;

    [TestFixture]
    public class MKM2019Tests
    {
        private const string CATEGORY = "MKM2019";

        [Test]
        [Category(CATEGORY)]
        public void ComplexInt32Test()
        {
            var a = Complex.Create(new Int32Concept()).Set(1, 9);
            var b = Complex.Create(new Int32Concept()).Set(4, 2);

            var c = Complex.Create(new Int32Concept()).Set(1, 9);
            var d = Complex.Create(new Int32Concept()).Set(6, 7);

            var e = Complex.Create(new Int32Concept()).Set(1, 9);
            var f = Complex.Create(new Int32Concept()).Set(9, 2);

            var g = Complex.Create(new Int32Concept()).Set(2, 0);
            var h = Complex.Create(new Int32Concept()).Set(1, 5);

            var sum = a + b;
            var diff = c - d;
            var prod = e * f;
            var qout = g / h;
        }
        [Test]
        [Category(CATEGORY)]
        public void ComplexSingleTest()
        {
            var a = Complex.Create(new SingleConcept()).Set(1, 9);
            var b = Complex.Create(new SingleConcept()).Set(4, 2);

            var c = Complex.Create(new SingleConcept()).Set(1, 9);
            var d = Complex.Create(new SingleConcept()).Set(6, 7);

            var e = Complex.Create(new SingleConcept()).Set(1, 9);
            var f = Complex.Create(new SingleConcept()).Set(9, 2);

            var g = Complex.Create(new SingleConcept()).Set(2, 0);
            var h = Complex.Create(new SingleConcept()).Set(1, 5);

            var sum = a + b;
            var diff = c - d;
            var prod = e * f;
            var qout = g / h;
        }
        [Test]
        [Category(CATEGORY)]
        public void ComplexDoubleTest()
        {
            var a = Complex.Create(new DoubleConcept()).Set(1, 9);
            var b = Complex.Create(new DoubleConcept()).Set(4, 2);

            var c = Complex.Create(new DoubleConcept()).Set(1, 9);
            var d = Complex.Create(new DoubleConcept()).Set(6, 7);

            var e = Complex.Create(new DoubleConcept()).Set(1, 9);
            var f = Complex.Create(new DoubleConcept()).Set(9, 2);

            var g = Complex.Create(new DoubleConcept()).Set(2, 0);
            var h = Complex.Create(new DoubleConcept()).Set(1, 5);

            var sum = a + b;
            var diff = c - d;
            var prod = e * f;
            var qout = g / h;
        }
        [Test]
        [Category(CATEGORY)]
        public void ComplexDecimalTest()
        {
            var a = Complex.Create(new DecimalConcept()).Set(1, 9);
            var b = Complex.Create(new DecimalConcept()).Set(4, 2);

            var c = Complex.Create(new DecimalConcept()).Set(1, 9);
            var d = Complex.Create(new DecimalConcept()).Set(6, 7);

            var e = Complex.Create(new DecimalConcept()).Set(1, 9);
            var f = Complex.Create(new DecimalConcept()).Set(9, 2);

            var g = Complex.Create(new DecimalConcept()).Set(2, 0);
            var h = Complex.Create(new DecimalConcept()).Set(1, 5);

            var sum = a + b;
            var diff = c - d;
            var prod = e * f;
            var qout = g / h;
        }
    };
}
