﻿namespace GenericComplex
{
    public class DoubleConcept : INumConcept<double>
    {
        public virtual double Add(double arg1, double arg2) => arg1 + arg2;
        public virtual double Mul(double arg1, double arg2) => arg1 * arg2;
        public virtual double Div(double arg1, double arg2) => arg1 / arg2;
        public virtual bool Et(double arg1, double arg2) => arg1 == arg2;
        public virtual bool Lt(double arg1, double arg2) => arg1 < arg2;
        public virtual double Neg(double arg) => -arg;
        public virtual double Abs(double arg) => arg < 0 ? Neg(arg) : arg;
        public virtual double Zero => 0;
        public virtual double One => 1;
    };
}
