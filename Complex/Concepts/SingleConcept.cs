﻿namespace GenericComplex
{
    public class SingleConcept : INumConcept<float>
    {
        public virtual float Add(float arg1, float arg2) => arg1 + arg2;
        public virtual float Mul(float arg1, float arg2) => arg1 * arg2;
        public virtual float Div(float arg1, float arg2) => arg1 / arg2;
        public virtual bool Et(float arg1, float arg2) => arg1 == arg2;
        public virtual bool Lt(float arg1, float arg2) => arg1 < arg2;
        public virtual float Neg(float arg) => -arg;
        public virtual float Abs(float arg) => arg < 0 ? Neg(arg) : arg;
        public virtual float Zero => 0;
        public virtual float One => 1;
    };
}
